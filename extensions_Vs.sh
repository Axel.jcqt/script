#!/bin/bash
# script

vert='\e[0;32m'
neutre='\e[0;m'
rouge='\e[0;31m'



echo -e  " ${vert}---------- Python Extensions ---------- ${neutre} "

(code --install-extension ms-python.python && code --install-extension njpwerner.autodocstring && code --install-extension fnando.linter && 
code --install-extension visualstudioexptteam.vscodeintellicode) && code --install-extension gruntfuggly.todo-tree || echo " ${rouge}---------- Echec de l'installation Python Extensions ---------- ${neutre} "


echo -e  " ${vert}---------- Java Extensions ---------- ${neutre} "

(code --install-extension vscjava.vscode-java-pack && code --install-extension spmeesseman.vscode-taskexplorer && code --install-extension massi.javascript-docstrings && code --install-extension sohibe.java-generate-setters-getters &&  code --install-extension github.copilot) || echo " ${rouge}---------- Echec de l'installation Java Extensions ---------- ${neutre} "


echo -e  " ${vert}---------- HTML Extensions ---------- ${neutre} "

code --install-extension ritwickdey.liveserver || echo " ${rouge}---------- Echec de l'installation HTML Extensions ---------- ${neutre} "

echo -e  " ${vert}---------- MYSQL Extensions ---------- ${neutre} "

code --install-extension cweijan.vscode-mysql-client2 || echo " ${rouge}---------- Echec de l'installation MYSQL Extensions ---------- ${neutre} "

echo -e  " ${vert}---------- Gitlab Extensions ---------- ${neutre} "

code --install-extension gitlab.gitlab-workflow || echo " ${rouge}---------- Echec de l'installation Gitlab Extensions ---------- ${neutre} "

echo -e  " ${vert}---------- Live Extensions ---------- ${neutre} "

code --install-extension ms-vsliveshare.vsliveshare || echo " ${rouge}---------- Echec de l'installation Live Extensions ---------- ${neutre} "


echo -e  " ${vert}---------- Extension nécessaire ---------- ${neutre} "

code --install-extension tomoki1207.pdf && code --install-extension shan.code-settings-sync && code --install-extension deerawan.vscode-dash && code --install-extension donjayamanne.githistory || echo " ${rouge}---------- Echec de l'installation PDF Extensions ---------- ${neutre} "


echo -e  " ${vert}---------- Installation de vscode et ses extensions réussi---------- ${neutre} "
