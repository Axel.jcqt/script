#!/bin/bash
#script

git config --global credential.helper cache                  # Garde le mots de passe et identifiant en cache
git config --global credential.helper "cache --timeout=5800" # pour 1h30

echo " Séléctionner l'option qui vous convient"
echo " 1 - Initialisation d'un dépot git"
echo " 2 - Push vos fichiers sur GitLab"
echo " 3 - Création d'une branche sur le dépot local et distant" 
echo " 4 - Merge deux branches"
echo " 5 - Faire un rebase de deux branches"
echo " 6 - Faire un Cherry-pick"
read choix


# On définié les différentes focntions

function push_gitlab (){
    clear
    git pull || git merge | code .
    git add . && echo "écrire votre message commit: " && read mess && git commit -m "$mess" && git push && clear
}

function creation_de_branches(){
    clear
    echo "Quel est le nom de la nouvelle branche: " && read branch
    #Création de la branche en local
    git switch -c $branch
    #Création de la branch sur le dépot
    git push --set-upstream origin $branch && git branch && clear
}

function merge_gitlab (){
    clear
    # Affichage des différentes branches à l'utilisateur
    echo " Voici les branches " && git branch
    # Choix des branches à Merge
    echo " Nom de la branche source" && read source1 && echo " Nom de la branche cible" && read cible
    # Mise à jour de l'origin et création de la branches dans le dépot local si elle n'existe pas
    git fetch origin && git checkout -b "$source1" "origin/$source1"
    git fetch origin
    git checkout "$cible"
    git merge "$source1" "$cible" && clear
    echo " Réglez les conflit en local sur Vscode"
    if [ $valider = 'y' ]
    then
        git add .
        git commit -m "Merge de la branche $source1 vers la branche $cible "
        git push origin "$cible"
        echo " Voulez vous supprimer la branche $source1 ? y/n" && read valider
        if [ $valider = 'y' ]
        then
            #Suppression de la branche en local
            git branch -d $source1
            #Suppression de la branche sur le dépot
            git push origin --delete $source1
            #Mise à jour du dépot distant
            git fetch --all --prune
            clear
        fi
    fi
}

function git_rebase () {
    clear
    git fetch
    echo " Voici les branches " && git branch
    echo "Quel est le nom de la branche source: " && read source
    echo "Quel est le nom de la branche cible: " && read cible
    git checkout $source
    git rebase $cible
    git push origin $source
    clear
    git fetch
    git add .
    # finir le rebase
    echo "Corriger les conflit et valider? y/n" && read valider
    if [ $valider = 'y' ] || [ $valider = 'Y' ]
    then
        git rebase --continue
        git push origin $source
        clear
    else
        git rebase --abort
        clear
    fi
}

function cherry_pick_gitlab () {
    git add .
    echo "Quel est la branche ciblé? " && read cible_cherry
    echo "Sur quel branche voulez vous récuperer le commit" && read dest_cherry
    git switch $cible_cherry
    git reflog
    echo "Quel est le SHA du commit que vous voulez récuperer:" && read SHA
    git switch $dest_cherry
    git cherry-pick $SHA
}

function initialisation_dépot_gitlab () {
    echo "Quel est le nom du dépot GitLab: " && read nom_depot
    # Demande le nom d'utilisateur et le mot de passe
    echo "Quel est votre nom d'utilisateur GitLab: " && read username
    mkdir $nom_depot
    cd $nom_depot
    # Création des fichier de base
    touch README.md
    touch .gitignore
    # Création des répertoire de base
    mkdir src
    mkdir doc
    mkdir bin
    mkdir data
    mkdir img
    git init
    echo "1---"
    git remote add $nom_depot https://gitlab.com/$username/$nom_depot.git
    echo "2---"
    git add .
    echo "3---"
    git commit -m "Initialisation du dépot"
    echo "4---"
    git push -u $nom_depot origin main
    }









# -------------------------------------------------------------------------------------------------------------- A partir d'ici on lance les fonctions en fonction du choix de l'utilisateur


if [ $choix = 1 ]
then
    # On appelle la fonction initialisation_dépot_gitlab
    initialisation_dépot_gitlab
fi
if [ $choix = 2 ]
then
    push_gitlab
fi
if [ $choix = 3 ]
then
    echo "Avez vous bien pensez a push vos fichiers sur GitLab avant? (O/N)"
    read reponse
    if [ $reponse = 'O' ] || [ $reponse = 'o'  ]
    then
        create_branch
    else
        #  Si la reponse est non, on push les fichiers sur GitLab
        if [ $reponse = 'N' ] || [ $reponse = 'n'  ]
        then
            echo "Nous allons pusher vos fichiers sur GitLab avant de créer une branche"
            push_gitlab
            clear
            echo "Nous allons maintenant créer une branche en toute sécurité"
            create_branch
        fi
    fi
fi
if [ $choix = 4 ]
then
    echo "Avez vous bien pensez a push vos fichiers sur GitLab avant? (O/N)"
    read reponse
    # Si la reponse est O ou o
    if [ $reponse = O  ] || [ $reponse = o  ]
    then
        merge_branch
    else
        if [ $reponse = 'N' ] || [ $reponse = 'n'  ]
        then
            echo "Nous allons pusher vos fichiers sur GitLab avant de faire un merge"
            push_gitlab
            clear
            echo "Nous allons maintenant faire un merge en toute sécurité"
            merge_branch
        fi
    fi
fi

if [ $choix = 5 ]
then
    echo "Avez vous bien pensez a push vos fichiers sur GitLab avant? (O/N)"
    read reponse
    # Si la reponse est O ou o
    if [ $reponse = O  ] || [ $reponse = o  ]
    then
        rebase_branch
    else
        if [ $reponse = 'N' ] || [ $reponse = 'n'  ]
        then
            echo "Nous allons pusher vos fichiers sur GitLab avant de faire un rebase"
            push_gitlab
            clear
            echo "Nous allons maintenant faire un rebase en toute sécurité"
            rebase_branch
        fi
    fi
fi
if [ $choix = 6 ]
then
    echo "Avez vous bien pensez a push vos fichiers sur GitLab avant? (O/N)"
    read reponse
    # Si la reponse est O ou o
    if [ $reponse = O  ] || [ $reponse = o  ]
    then
        cherry_pick
    else
        if [ $reponse = 'N' ] || [ $reponse = 'n'  ]
        then
            echo "Nous allons pusher vos fichiers sur GitLab avant de faire un cherry-pick"
            push_gitlab
            clear
            echo "Nous allons maintenant faire un cherry-pick en toute sécurité"
            cherry_pick
        fi
    fi
fi







# Created by Jacquet Axel
